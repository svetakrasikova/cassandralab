#-*- coding: utf-8 -*-
from cassandra.cluster import Cluster

#cluster = Cluster(['54.185.17.217'])
cluster = Cluster(['54.185.23.182'])
#cluster = Cluster(['54.185.30.189'])
session = cluster.connect()
session.execute("USE group3;")

print "Connected to database. Reading in movie dump…"

actor2filmed_in = {}
descriptions = []
genres_and_ratings = []
#### 1. parse the table
maxFilmedIn = 1
for line in open("movies_dump.txt"):
	year, title, rating, genres, actors = line[:-1].split("\t");
	
	if int(year) <= 1970 or int(year) > 1991:
		continue

	title = title.replace("\'", " ")
	actors = [actor.strip() for actor in actors.split("|") if actor.strip()]
	actors = [actor.replace("\'", " ") for actor in actors]
	genres = [genre.strip() for genre in genres.split("|") if genre.strip() and len(genre) < 30]
	for actor in actors:
		actor2filmed_in.setdefault(actor, 0)
		actor2filmed_in[actor] += 1
		if actor2filmed_in[actor] > maxFilmedIn:
			maxFilmedIn = actor2filmed_in[actor]
	descriptions += [(title, year, rating, "{'"+"','".join(genres)+"'}", "{'"+"','".join(actors)+"'}")]
	for genre in genres:
		genres_and_ratings += [(genre, float(rating), title)]
genres_and_ratings.sort()



session.execute("DROP TABLE IF EXISTS actors;")
session.execute("CREATE TABLE actors (name varchar PRIMARY KEY, filmed_in int);")
session.execute("DROP TABLE IF EXISTS popularity;")
session.execute("CREATE TABLE popularity (fake_field int, filmed_in int, name varchar, PRIMARY KEY (fake_field, filmed_in, name)) WITH CLUSTERING ORDER BY (filmed_in DESC);")
#session.execute("DROP TABLE IF EXISTS popularity_new;")
#session.execute("CREATE TABLE popularity_new (fake_field int PRIMARY KEY, "+",".join(["in%03d set<text>" % _ for _ in range(1, maxFilmedIn+1)])+");")
#print "Max filmed_in %d" % maxFilmedIn
#session.execute("INSERT INTO popularity_new (fake_field) VALUES (1);")

session.execute("DROP TABLE IF EXISTS ratings;")
session.execute("CREATE TABLE ratings (genre varchar, rating float, title varchar, PRIMARY KEY (genre, rating, title)) WITH CLUSTERING ORDER BY (rating DESC);")
session.execute("DROP TABLE IF EXISTS movie_desc;")
session.execute("CREATE TABLE movie_desc (title varchar PRIMARY KEY, year int, rating float, genres set<text>, actors set<text>);")

if 1 and "uploading actors":
	BATCH_SIZE = 5000
	print "uploading actors %d" % len(actor2filmed_in)
	actors2upload = [(actor, filmed_in) for actor, filmed_in in actor2filmed_in.items()]
	start_index = 0
	while start_index < len(actors2upload):
		if 1:
			data_chunk = actors2upload[start_index:start_index + BATCH_SIZE]
			chunk = ["INSERT INTO actors (name, filmed_in) VALUES ('%s', %d)" % \
															(actor, filmed_in) for actor, filmed_in in data_chunk]
			chunk += ["INSERT INTO popularity (fake_field, name, filmed_in) VALUES (1, '%s', %d)" % \
															(actor, filmed_in) for actor, filmed_in in data_chunk]
#			chunk += ["UPDATE popularity_new SET in%03d = in%03d + {'%s'} WHERE fake_field = 1;" % \
#															(filmed_in, filmed_in, actor) for actor, filmed_in in data_chunk]
			command = "BEGIN BATCH %s \n APPLY BATCH;" % "\n".join(chunk)
			session.execute(command)
			print "\t", start_index, start_index + BATCH_SIZE, "done"
		start_index += BATCH_SIZE

if 1 and "uploading ratings":
	BATCH_SIZE = 4000
	print "uploading ratings %d" % (len(genres_and_ratings))
	start_index = 0
	while start_index < len(genres_and_ratings):
		if 1:
			chunk = genres_and_ratings[start_index:start_index + BATCH_SIZE]
			chunk = ["INSERT INTO ratings  (genre, rating, title) VALUES ('%s', %.3f, '%s')" % \
					  (genre, rating, title) for genre, rating, title in chunk]
			command = "BEGIN BATCH \n %s \n APPLY BATCH;" % "\n".join(chunk)
			#print command
			session.execute(command)
			print "\t", start_index, start_index + BATCH_SIZE, "done"
		start_index += BATCH_SIZE

if 1 and "uploading movies":
	BATCH_SIZE = 2000
	print "uploading movies %d" % (len(descriptions))
	start_index = 0
	while start_index < len(descriptions):
		if 1:
			chunk = descriptions[start_index:start_index + BATCH_SIZE]
			chunk = ["INSERT INTO movie_desc  (title, year, rating, genres, actors) VALUES ('%s', %s, %s, %s, %s)" % \
										(title, year, rating, genres, actors) for title, year, rating, genres, actors in chunk]
			command = "BEGIN BATCH \n %s \n APPLY BATCH;" % "\n".join(chunk)
			#print command
			session.execute(command)
			print "\t", start_index, start_index + BATCH_SIZE, "done"
		start_index += BATCH_SIZE	 


session.shutdown()
cluster.shutdown()		  
